;; require helm 就可以直接 定义变量了 
;; 例如 (helm-autoresize-mode t) , (define-key helm-map (kbd "C-z")  'helm-select-action)
;; 否则需要 setq
(require 'helm)

(require 'helm-config)
;;(helm-mode 1)

;; key binding
;; (global-set-key (kbd "M-x") 'helm-M-x)
(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to do persistent action
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
(define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

;; resize
(setq helm-autoresize-mode t)   ;; 自动resize
;; 如果max 和min 设置相同值,则 size固定
;;(setq helm-autoresize-max-height 40)  
;;(setq heml-autoresize-min-height 20)


;; Fuzzy matching 
;; helm-recentf: Enable by setting `helm-recentf-fuzzy-match` to t.
;(setq helm-recentf-fuzzy-match t)

;; helm-mini: Enable by setting `helm-buffers-fuzzy-matching` and `helm-recentf-fuzzy-match` to t.
;; helm-buffers-list: Enable by setting `helm-buffers-fuzzy-matching` to t.
(setq helm-buffers-fuzzy-matching t)

;; helm-find-files: Enable by default.
;; helm-locate: Enable by setting `helm-locate-fuzzy-match` to t.
;(setq helm-locate-fuzzy-match t)

;; helm-M-x: Enable by setting `helm-M-x-fuzzy-match` to t.
;(setq helm-M-x-fuzzy-match t)

;; helm-semantic: Enable by setting `helm-semantic-fuzzy-match` to t.
;(setq helm-semantic-fuzzy-match t)

;; helm-imenu: Enable by setting `helm-imenu-fuzzy-match` to t.
(setq helm-imenu-fuzzy-match t)
;; helm-apropos: Enable by setting `helm-apropos-fuzzy-match` to t.
;; helm-lisp-completion-at-point: Enable by setting `helm-lisp-fuzzy-completion` to t.

;; You can also enable fuzzy matching globally in all functions helmized by `helm-mode` with `helm-mode-fuzzy-match` and `helm-completion-in-region-fuzzy-match`.
;(setq helm-mode-fuzzy-match t)
;(setq helm-completion-in-region-fuzzy-match t)


(provide 'init-helm)
