(require 'python)
;; 将 python27 的根目录路径 写进emacs环境变量
(setq pythonversion "27")
(setq program-path "d:/")
(setenv "PATH" (concat (concat program-path "python" pythonversion ";") (concat program-path "python" pythonversion "/Lib;")
    (concat program-path "python" pythonversion "/Scripts;") 
   (getenv "PATH")))

;(setenv "PYTHONPATH" (concat (concat program-path"python"pythonversion)))
(setenv "PYTHONPATH" (concat (concat program-path "python" pythonversion ";") (concat program-path "python" pythonversion "/Lib;")
    (concat program-path "python" pythonversion "/Scripts;") ".;"
   (getenv "PYTHONPATH")))

(when (string-equal system-type "windows-nt")
   (setq exec-path (split-string (getenv "PATH") ";"))
  ; (setq exec-path (append exec-path 
  ;   '(concat (concat program-path "python" pythonversion ";")
  ;    (concat program-path "python" pythonversion "/Lib;")
  ;   (concat program-path "python" pythonversion "/Scripts;"))))
)


(setq python-python-command "D:/Python27/python.exe"
      python-shell-exec-path "D:/Python27/python.exe"
)

(setq
 python-shell-interpreter "D:/Python27/python.exe"
 python-shell-interpreter-args "-i D:\\Python27\\Scripts\\ipython.exe"
 python-shell-prompt-regexp "In \\[[0-9]+\\]: "
 python-shell-prompt-output-regexp "Out\\[[0-9]+\\]: "
 python-shell-completion-setup-code
   "from IPython.core.completerlib import module_completion"
 python-shell-completion-module-string-code
   "';'.join(module_completion('''%s'''))\n"
 python-shell-completion-string-code
   "';'.join(get_ipython().Completer.all_completions('''%s'''))\n")


;; 用于切换 python 版本
(defun zxy-switch-python-version (&optional (pver nil))
  "Swith python version at runtime"
  (interactive)
  (if (equal nil pver)
      (let ((tempver (read-string (format "[zxy] Swith to which version of python (default %s)? "
                                          pythonversion) nil nil pythonversion nil)))
        (setq pver tempver)))
  (unless (file-exists-p (concat program-path"python"pver""))
    (error (format "[zxy] Cannot find python%s!" pver)))
  (message (format "[zxy] Switch to python%s." pver))
  (setq pythonversion pver)
  (setenv "PATH" (concat (concat program-path"python"pythonversion";") (getenv "PATH"))))
  
;; emacs python

(defun zxy-python-compile ()
  "Use compile to run python programs"
  (interactive)
  (compile (concat "python " (buffer-name))))
(setq compilation-scroll-output t)
(define-key python-mode-map "\C-cc" 'zxy-python-compile)

(provide 'init-python)
