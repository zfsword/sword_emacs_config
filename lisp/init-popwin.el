(require 'popwin)
(popwin-mode 1)
(global-set-key (kbd "C-c p") popwin:keymap)
(provide 'init-popwin)
