(setq fiplr-root-markers '(".git" ".svn" "Assets"))
(setq fiplr-ignored-globs '((directories (".git" ".svn" "Temp"))
                            (files ("*.jpg" "*.png" "*.zip" "*~" "*.asset" "*.meta" "*.mat" "*.prefab"))))

(global-set-key (kbd "C-x f") 'fiplr-find-file)

(provide 'init-fiplr)
