(add-hook 'python-mode-hook 'anaconda-mode)
(add-hook 'python-mode-hook 'eldoc-mode)

(eval-after-load "company"
 '(progn
   (add-to-list 'company-backends 'company-anaconda)))

(provide 'init-anaconda-mode)