(require 'omnisharp)

(require 'company)
(require 'csharp-mode)
(defun my-csharp-mode ()
 (eval-after-load 'company
     '(add-to-list 'company-backends 'company-omnisharp))
 (omnisharp-mode)
 (company-mode)
 (flycheck-mode)
 (turn-on-eldoc-mode))
(add-hook 'csharp-mode-hook 'my-csharp-mode)

;; omnisharp
;(add-hook 'csharp-mode-hook 'omnisharp-mode)

;(setq omnisharp-company-strip-trailing-brackets nil)

;(add-hook 'after-init-hook #'global-flycheck-mode)
;(add-hook 'flycheck-mode-hook 'flycheck-color-mode-line-mode)

;(add-to-list 'company-backends 'company-omnisharp)

;(eval-after-load 'company
 ;	'(add-to-list 'company-backends 'company-omnisharp))


;(setenv "PATH" (concat  "D:\\Emacs\\home\\curl\\curl.exe"
;;   (getenv "PATH")))

;(setq
	;(setq omnisharp--curl-executable-path "D:\\Emacs\\home\\curl\\curl.exe")
    ;(setq omnisharp--curl-executable-path "D:\\Program Files\\PortableGit\\bin\\curl.exe")
	(setq omnisharp--curl-executable-path "d:/Emacs/home/curl/curl.exe")
    ;(setq omnisharp--curl-executable-path "~/curl/curl.exe")

	;(setq omnisharp-server-executable-path "D:/Vim/vimfiles/pathogen/bundle/omnisharp-vim/server/OmniSharp/bin/Release/OmniSharp.exe ")
	;(setq omnisharp-server-executable-path "D:/OmniSharp/Release/OmniSharp.exe")
	(setq omnisharp-server-executable-path "D:/OmniSharp/Debug/OmniSharp.exe")
	;(setq omnisharp-server-executable-path "D:\\OmniSharp\\Release\\OmniSharp.exe")

	;(setq omnisharp--windows-curl-tmp-file-path "~/temp/omnisharp-tmp-file.cs")
	(setq omnisharp--windows-curl-tmp-file-path "D:/Emacs/home/temp/omnisharp-tmp-file.cs")
	;(setq omnisharp--windows-curl-tmp-file-path "/omnisharp-tmp-file.cs")
	;(setq omnisharp--windows-curl-tmp-file-path "D:\\Emacs\\home\\temp\\omnisharp-tmp-file.cs")

;)
;(setq omnisharp-host "http://127.0.0.1:2000/")
(setq omnisharp-debug t)

(provide 'init-omnisharp)
