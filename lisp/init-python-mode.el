
;; 将 python27 的根目录路径 写进emacs环境变量
(setq pythonversion "27")
(setq program-path "d:/")
(setenv "PATH" (concat (concat program-path "python" pythonversion ";") (concat program-path "python" pythonversion "/Lib;")
    (concat program-path "python" pythonversion "/Scripts;") 
   (getenv "PATH")))

;(setenv "PYTHONPATH" (concat (concat program-path"python"pythonversion)))
(setenv "PYTHONPATH" (concat (concat program-path "python" pythonversion ";") (concat program-path "python" pythonversion "/Lib;")
    (concat program-path "python" pythonversion "/Scripts;") (concat program-path "python" pythonversion "/Lib/site-packages;") ".;"
   (getenv "PYTHONPATH")))

(when (string-equal system-type "windows-nt")
   (setq exec-path (split-string (getenv "PATH") ";"))
  ; (setq exec-path (append exec-path 
  ;   '(concat (concat program-path "python" pythonversion ";")
  ;    (concat program-path "python" pythonversion "/Lib;")
  ;   (concat program-path "python" pythonversion "/Scripts;"))))
)

(require 'python-mode)

;; python-mode settings
(autoload 'python-mode "python-mode" "Python editing mode." t)
(add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))
(add-to-list 'interpreter-mode-alist '("python" . python-mode))


;; Make sure, the directory where python.exe resides in in the PATH-variable.
(setq py-python-command "D:/Python27/python.exe")
(setq py-python-command-args "")
;
;
;; 其他版本设置
;(setq py-python2-command "D:/Python27/python.exe")
;(setq py-python2-command-args 
;
;(setq py-python3-command "Path/To/Python3")

;; If `t’, py-shell will use `py-shell-local-path’ instead of default Python.
;; Making switch between several virtualenv’s easier, `python-mode’ should deliver an installer, so named-shells pointing to virtualenv’s will be available.
(setq py-shell-local-path "D:/Python27/python.exe" py-use-local-default t)

;; When `t’, execution with kind of Python specified in `py-shell-name’ is enforced, possibly shebang doesn’t take precedence.
(setq py-force-py-shell-name-p t)

;; Allow execution of code inside blocks started by “if __name__== ‘__main__’:”. Default is non-nil
(setq py-if-name-main-permission-p t)
;; switch to the interpreter after executing code
(setq py-switch-buffers-on-execute-p t)

;; don't split windows
(setq py-split-windows-on-execute t)
;; try to automagically figure out indentation
(setq py-smart-indentation t)

 ; (defun run-python-once ()
 ;   (remove-hook 'python-mode-hook 'run-python-once)
 ;   (run-python))
 ; (add-hook 'python-mode-hook 'run-python-once)

(add-hook 'python-mode 'run-python)

;; 由于现阶段IPython开发者 没人懂 elisp了，所以 ipython.el 过时，无法使用
;; use IPython
;(require 'ipython)
(setq py-ipython-command "D:/Python27/Scripts/ipython.exe") ; discard this line Ipython is already in your PATH
(setq py-ipython-command-args "D:/Python27/Scripts/ipython-script.py")
;; use the wx backend, for both mayavi and matplotlib
;(setq py-ipython-command-args
;   '("--gui=wx" "--pylab=wx" "-colors" "Linux"))

;; A PATH/TO/EXECUTABLE or default value `py-shell' may look for, if no shell is specified by command.
;; On Windows default is C:/Python27/python --there is no garantee it exists, please check your system--
;; Else python
;(setq py-shell-name "D:/Python27/python")
(setq py-shell-name "D:/Python27/Scripts/ipython.exe")
;(setq-default py-shell-name "D:/Python27/Scripts/ipython.exe")
;(setq-default py-which-bufname "D:/Python27/Scripts/ipython.exe")
;
;
;; Where to find pdb.py. Edit this according to your system.If you ignore the location `M-x py-guess-pdb-path’ might display it.
(setq py-pdb-path "D:/Python27/Lib/pdb.py")
(setq py-pdb-executable "D:/Python27/Scripts/ipdb.exe")


(provide 'init-python-mode)
