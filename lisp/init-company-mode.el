(autoload 'company-mode "company" nil t)
;	(autoload 'company-mode "omnisharp-auto-complete-actions" nil t)

(add-hook 'after-init-hook 'global-company-mode)

(provide 'init-company-mode)
