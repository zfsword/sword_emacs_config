
(avy-setup-default)

;; Input one char, jump to it with a tree.
(global-set-key (kbd "C-;") 'avy-goto-char)

;; Input two consecutive chars, jump to the first one with a tree.
(global-set-key (kbd "C-'") 'avy-goto-char-2)

;; Input zero chars, jump to a line start with a tree.
(global-set-key (kbd "M-g f") 'avy-goto-line)

;; Input one char at word start, jump to a word start with a tree.
(global-set-key (kbd "M-g w") 'avy-goto-word-1)

;; Input zero chars, jump to a word start with a tree.
(global-set-key (kbd "M-g e") 'avy-goto-word-0)



;; The default overlay display style.
;;    pre: - full path before target, leaving all original text.
;;    at: - single character path on target, obscuring the target.
;;    at-full: full path on target, obscuring the target and the text behind it.
;;    post: full path after target, leaving all original text.
;(setq avy-style 'post)
;
;; The alist of overlay display styles for each command.
(setq avy-styles-alist '((avy-goto-word-0 . pre)
             (avy-goto-word-1 . pre)
             ))

;; nil: use only the selected window
;; t: use all windows on the selected frame
;; all-frames: use all windows on all frames
;(setq avy-all-windows nil)

(provide 'init-avy)
