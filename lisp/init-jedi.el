(add-hook 'python-mode-hook 'jedi:setup)
;(add-hook 'python-mode-hook 'jedi:ac-setup)
(setq jedi:complete-on-dot t)                 ; optional

(provide 'init-jedi)
