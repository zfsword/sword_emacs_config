(when (require 'elpy nil t)
  (elpy-enable))

(setq elpy-rpc-backend "jedi")
;(setq elpy-rpc-python-command "python2")  
(setq elpy-rpc-python-command "D:/Python27/python.exe")  
(elpy-use-ipython)  

;; 将默认的 M-TAB 修改, 也可以使用C-i
;(define-key elpy-mode-map (kbd "TAB") 'elpy-company-backend)

(provide 'init-elpy)
