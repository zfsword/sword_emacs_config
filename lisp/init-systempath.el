;; 将 python27 的根目录路径 写进emacs环境变量
;(setq pythonversion "27")
;(setq program-path "d:/")
;(setenv "PATH" (concat (concat program-path "python" pythonversion ";") (concat program-path "python" pythonversion "/Lib;")
;    (concat program-path "python" pythonversion "/Scripts;") 
;   (getenv "PATH")))
;
;;(setenv "PYTHONPATH" (concat (concat program-path"python"pythonversion)))
;(setenv "PYTHONPATH" (concat (concat program-path "python" pythonversion ";") (concat program-path "python" pythonversion "/Lib;")
;    (concat program-path "python" pythonversion "/Scripts;") ".;"
;   (getenv "PYTHONPATH")))

;; helm locate 需要用到Everything/es.exe 在windows下查找文件, es 需要超管权限,还需要开启everything.exe
(setenv "PATH" (concat "f:/Program Files/Firefox/" ";" "D:/emacs/emacs-24.5-bin-i686-mingw32/bin" ";"
              "f:/program files/Everything/" ";" (getenv "PATH") ) )

(when (string-equal system-type "windows-nt")
    (setq exec-path (split-string (getenv "PATH") ";"))
    ;; let emacs know Firefox's path
    ;(add-to-list 'exec-path "f:/Program Files/Firefox/" ";")
   (setq exec-path (append exec-path 
     '(concat "f:/program files/firefox/" ";"
              "D:/emacs/emacs-24.5-bin-i686-mingw32/bin" ";"
              "f:/program files/Everything/" ";")
              ))
  ; (setq exec-path (append exec-path 
  ;   '(concat (concat program-path "python" pythonversion ";")
  ;    (concat program-path "python" pythonversion "/Lib;")
  ;   (concat program-path "python" pythonversion "/Scripts;"))))
)

(provide 'init-systempath)

