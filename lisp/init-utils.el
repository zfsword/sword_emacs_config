;; after-load
(defmacro after-load (feature &rest body)
"After FEATURE is loaded, evaluate BODY."
(declare (indent defun))
    `(eval-after-load ,feature
    '(progn ,@body)))

;; duckduckgo search
;(defun duckduckgo (what)
;  "Use ddg to search for WHAT."
;  (interactive "sSearch: ")
;  (browse-url-firefox (concat "https://duckduckgo.com/?t=lm&q="
;                              (url-unhex-string what))))
    
(provide 'init-utils)
