(require 'evil-vimish-fold)

(vimish-fold-global-mode 1)

; (global-set-key (kbd "M-g c") #'vimish-fold)
; (global-set-key (kbd "M-g d") #'vimish-fold-delete)
               
(provide 'init-vimish-fold)