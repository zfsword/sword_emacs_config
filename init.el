;; init.el
;; 把目录lisp/添加到搜索路径中去
(add-to-list
'load-path
(expand-file-name "lisp" user-emacs-directory))

(add-to-list `load-path "~/.emacs.d/elpa")
;; 下面每一个被require的feature都对应一个lisp/目录下的同名
;; elisp文件，例如init-utils.el、init-elpa.el
(require 'init-utils) ;; 为加载初始化文件提供一些自定义的函数和宏
(require 'init-systempath) ;; 设置环境变量
(require 'init-elpa) ;; 加载ELPA，并定义了require-package函数
(require 'init-vimish-fold)
(require 'init-evil) ;; 加载evil初始化
(require 'init-avy)  ;; 类似ace-jump
(require 'init-smex)
(require 'init-helm)
(require 'init-multiple-cursors)
(require 'init-auto-complete)
(require 'init-company-mode)
;(require 'init-python)
;(require 'init-python-mode)
;(require 'init-anaconda-mode)
;(require 'init-elpy)
;(require 'init-jedi)
;(require 'init-company-jedi)
;(require 'init-pymacs)
(require 'init-omnisharp)
(require 'init-flycheck)
(require 'init-yasnippet)
;(require 'init-tabbar-ruler)
(require 'init-fiplr)
(require 'init-popwin)
(require 'init-neotree)

;; (require 'init-fonts) ;; 以Server-Client模式启动时需额外设置字体
;; (require 'init-editing-utils) ;; 一些顺手的小工具
;;(require 'init-markdown)
;;(require 'init-auctex)
;;
;;
;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(setq ediff-diff-options "--text")
;(add-hook 'after-init-hook 'server-start) ;; server模式启动
(server-start)
;;
;;
;; 显示debug出错信息
;(setq debug-on-error t)

;;设置 utf 编码
;(setq utf-translate-cjk-mode nil) ; disable CJK coding/encoding (Chinese/Japanese/Korean characters)
(prefer-coding-system 'utf-8)
;(prefer-coding-system 'gbk')

;(set-language-environment 'Chinese-gbk)  ; 系统环境编码
;(set-language-environment "UTF-8")
;(setq current-language-environment "UTF-8") ; 此方式同 set-language-environment

;(setq locale-coding-system 'utf-8)   ; Coding system to use with system messages. Also used for decoding keyboard input on X Window system.
;(setq coding-system-for-write 'utf-8)
;(setq coding-system-for-read 'utf-8)
;(set-terminal-coding-system 'utf-8)
(set-terminal-coding-system 'chinese-gbk)   ; 终端编码
;(setq default-process-coding-system '(utf-8-unix . utf-8-unix)) ;进程输出输入编码
;(setq default-process-coding-system '(utf-8-dos . utf-8-dos)) ;进程输出输入编码
;(setq default-sendmail-coding-system 'utf-8-unix)               ;发送邮件编码

;(unless (eq system-type 'windows-nt)
;    (set-selection-coding-system 'utf-8))
(set-selection-coding-system 'utf-8)

(set-keyboard-coding-system 'utf-8)
;; (set-keyboard-coding-system 'utf-8-mac) ; For old Carbon emacs on OS X only


;; MS Windows clipboard is UTF-16LE
;(set-clipboard-coding-system 'utf-8)
(set-clipboard-coding-system 'utf-16le-dos)

(setq-default pathname-coding-system 'utf-8)
(setq file-name-coding-system 'gbk)              ;文件名编码


(set-buffer-file-coding-system 'utf-8)
;(setq buffer-file-coding-system 'utf-8)
;(setq revert-buffer-with-coding-system 'GBK)

;(modify-coding-system-alist 'process "*" 'utf-8)
;
;
;(setq auto-coding-regexp-alist
;  (delete (rassoc 'utf-16be-with-signature auto-coding-regexp-alist)
;  (delete (rassoc 'utf-16le-with-signature auto-coding-regexp-alist)
;  (delete (rassoc 'utf-8-with-signature auto-coding-regexp-alist)
;          auto-coding-regexp-alist))))


;;(setq default-input-method "chinese-py") ; 默认输入法


;; 设置字体
(add-to-list 'default-frame-alist
                       '(font . "YaHei Consolas Hybrid-10"))

(set-face-attribute 'default t :font "YaHei Consolas Hybrid-10")

;; 设置主题,需要先安装monokai
(load-theme 'monokai t)

;; 常规设置
;(menu-bar-mode -1) ;; 不显示菜单
;(tool-bar-mode -1) ;; 不显示工具栏
;(scroll-bar-mode -1) ;; 不显示滚动条
(column-number-mode t) ;; 显示行数和列数
(global-linum-mode t) ;; 显示行号
(setq x-select-enable-clipboard t) ;; 允许与系统剪切板交互
;(setq default-directory "d:/home/") ;; 设置默认目录
;(appt-activate 1) ;; 启动日历提醒
(setq require-final-newline t) ;; 编辑文件的最后一个字符是回车
;(setq bookmark-default-file "~/.emacs.d/.emacs.bmk") ;; 书签文件位置
(fset 'yes-or-no-p 'y-or-n-p) ;; 简化确认时的输入
;(global-set-key "\r" 'newline-and-indent) ;; 回车默认缩进
;(electric-indent-mode nil)  ;; 24.4 关闭新增自动缩进
;(setq inhibit-startup-message t) ;; 不显示启动时的界面
(show-paren-mode t) ;; 显示括号的对应反括号
(setq show-paren-style 'mixed) ;; 设置显示括号的样式
(electric-pair-mode t)  ;; 自动补全括号，引号
(mouse-avoidance-mode 'animate) ;; 光标与鼠标重合时，移开鼠标
;(setq frame-title-format "%b -- Do It Yourself") ;; 设置标题
(setq default-tab-width 4) ;; Tab 的宽度为 4
(setq-default indent-tabs-mode nil) ;; 用空格代替Tab, 另外有命令 M-x tabify  M-x untabif
(set-frame-position (selected-frame) 50 1) ;; 设置初始位置,单位pixel
(set-frame-size (selected-frame) 250 57) ;; 设置初始的窗口宽高,单位Character
(desktop-save-mode t) ;; 保存所有已开窗口,以便下次恢复
(global-auto-revert-mode t) ;; 自动刷新文件

;;临时文件
;(setq auto-save-default nil) ;; 默认值是t， 要关闭直接用nil更改默认值
(setq auto-save-interval 50) ;; 默认300 字符自动保存
(setq auto-save-timeout 120) ;; 默认30秒自动保存
(setq auto-save-file-name-transforms
      `((".*" ,"~/.emacs.d/auto-save-list/" t)))
;; `((".*" ,temporary-file-directory t)))  ;; 默认win7 C:\Users\Administrator\AppData\Local\Temp

;;备份设置
(setq make-backup-files nil) ;; 不产生备份文件
;(setq backup-directory-alist
;      `((".*" . ,temporary-file-directory)))
   ;;emacs还有一个自动保存功能，默认在~/.emacs.d/auto-save-list里，这个非常有用，我这里没有改动，具体可以参见Sams teach yourself emacs in 24hours(我简称为sams24)
    ;;启用版本控制，即可以备份多次
    ;(setq version-control t)
    ;;备份最原始的版本两次，记第一次编辑前的文档，和第二次编辑前的文档
    ;(setq kept-old-versions 2)
    ;;备份最新的版本五次，理解同上
    ;(setq kept-new-versions 5)
    ;;删掉不属于以上7中版本的版本
    ;(setq delete-old-versions t)
    ;;设置备份文件的路径
    ;(setq backup-directory-alist '(("." . "~/.emacs.tmp")))
    ;;备份设置方法，直接拷贝
    ;(setq backup-by-copying t)



(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(browse-url-browser-function (quote browse-url-firefox)))


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(provide 'init)
